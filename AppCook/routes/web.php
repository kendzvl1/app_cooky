<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace'=>'Local'],function(){

    Route::get('/', 'ClientController@getlogin');
    Route::get('register','ClientController@getregister');
    Route::get('home','ClientController@gethome');
    Route::get('mypost','ClientController@getmypost');
    Route::get('detail','ClientController@getdetail');
    
    Route::group(['prefix'=>'admin'],function(){
       
      
            Route::get('user','LoginController@getuser');
            Route::get('product','LoginController@getproduct');
     

        Route::group(['prefix'=>'login'],function(){

            Route::get('/','LoginController@getlogin');
        });
    
        Route::group(['prefix'=>'register'],function(){
            Route::get('/','LoginController@getregister');
        });

    });
 

});

