<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::namespace('Admin')->group(function () {

        Route::post('register','Login@postRegister');
        Route::post('login','Login@postlogin');
        Route::get('user','Login@getacc');
        Route::get('user/{id}','Login@getoneuser');
        Route::put('user/{id}','Login@updateuser');
        Route::delete('user/{id}','Login@deleteuser');
        Route::get('personal/{id}','PersonalController@getproductlist');

        Route::apiResource('type','TypeController');
        Route::apiResource('product','ProductController')->except('create','edt');
});



