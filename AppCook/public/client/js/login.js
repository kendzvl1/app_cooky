$(document).ready(function() {
  toastr.options = {
    closeButton: true,
    debug: false,
    newestOnTop: true,
    progressBar: true,
    positionClass: "toast-top-right",
    preventDuplicates: false,
    showDuration: "1000",
    hideDuration: "1000",
    timeOut: "3000",
    extendedTimeOut: "1000",
    showEasing: "swing",
    hideEasing: "linear",
    showMethod: "fadeIn",
    hideMethod: "fadeOut"
  };

  setCookie("email", "");
  setCookie("name", "");
  setCookie("id", "");

  $("#login").click(async function(e) {
    e.preventDefault();
    //lay du lieu tu giao dien
    var email = $("#email").val();
    var password = $("#password").val();

    if (email.length == 0) {
      toastr.error("Nhập vào email");
    } else if (password.length == 0) {
      toastr.error("Nhập vào mật khẩu");
    } else {
      //lấy kết quả trả về từ API
      const getInfo = () => {
        return new Promise(resolve => {
          $.ajax({
            url: "http://hyperzone.xyz/AppCook/api/login",
            type: "POST",
            dataType: "json",
            data: { email, password },
            success: function(Result) {
              return resolve(Result);
            },
            error: function(xhr, textStatus, errorThrown) {
              console.log(textStatus);
            }
          });
        });
      };
      //biến chứa kết quả trả về từ API
      const info = await getInfo();
      //kiểm tra kết quả trả về
      if (info.status == false) {
        toastr.error(info.message);
      } else {
        let { data } = info;
        setCookie("email", data.email);
        setCookie("name", data.name);
        setCookie("id", data.id);
        toastr.success("Success sign in!");
        setTimeout(function() {
          
         // window.location='{{asset("home")}}';
         window.location.href = "../home";
        }, 1500);
      }
    }
  });
});

function setCookie(name, value) {
  document.cookie = `${name} = ${value} ;path=/`;
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

(function($) {
  "use strict";

  /*==================================================================
  [ Validate ]*/
  var input = $(".validate-input .input100");

  $(".validate-form").on("submit", function() {
    var check = true;

    for (var i = 0; i < input.length; i++) {
      if (validate(input[i]) == false) {
        showValidate(input[i]);
        check = false;
      }
    }

    return check;
  });

  $(".validate-form .input100").each(function() {
    $(this).focus(function() {
      hideValidate(this);
    });
  });

  function validate(input) {
    if ($(input).attr("type") == "email" || $(input).attr("name") == "email") {
      if (
        $(input)
          .val()
          .trim()
          .match(
            /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/
          ) == null
      ) {
        return false;
      }
    } else {
      if (
        $(input)
          .val()
          .trim() == ""
      ) {
        return false;
      }
    }
  }

  function showValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).addClass("alert-validate");
  }

  function hideValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).removeClass("alert-validate");
  }
})(jQuery);
