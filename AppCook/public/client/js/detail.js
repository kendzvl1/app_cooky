$(document).ready(function() {
  var id = getUrlParameter("id");

  if (id !== "" && id !== undefined) {
    $.ajax({
      url: "http://hyperzone.xyz/AppCook/api/product/" + id,
      method: "GET",
      success: function(res) {
        var item = res.data;
        var html = "";
        html =
          '<div class="col-lg-12">' +
          '<div style="text-align: center; margin-bottom: 20px;">' +
          "<h2>" +
          item.title +
          "</h2>" +
          "</div>" +
          "</div>" +
          '<div class="col-lg-2 col-md-3 order-md-1 order-2">' +
          "</div>" +
          '<div class="col-lg-8 col-md-7 order-md-1 order-1">' +
          '<div class="blog__details__text">' +
          '<img src="' +
          item.img +
          '" alt="">' +
          "<p>" +
          item.content +
          "</p>" +
          "<h4>Nguyên liệu:</h4>" +
          "<p>" +
          item.resources +
          "</p>" +
          "<h4>Cách nấu:</h4>" +
          "<p>" +
          item.tutorial +
          "</p>" +
          "</div>" +
          '<div class="blog__details__content">' +
          '<div class="row">' +
          '<div class="col-lg-6">' +
          '<div class="blog__details__author">' +
          '<div class="blog__details__author__pic">' +
          '<img src="img/blog/details/details-author.jpg" alt="">' +
          "</div>" +
          '<div class="blog__details__author__text">' +
          "<h6>" +
          item.name +
          "</h6>" +
          "<span>" +
          item.created_at +
          "</span>" +
          "</div>" +
          "</div>" +
          "</div>" +
          "</div>" +
          "</div>" +
          "</div>";
        $(".detail_post").prepend(html);
      },
      error: function(err) {
        alert("Can't get data !");
        console.log(err);
      }
    });
  }
});

function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split("&"),
    sParameterName,
    i;
  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split("=");

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined
        ? true
        : decodeURIComponent(sParameterName[1]);
    }
  }
}
