$(document).ready(function() {
  toastr.options = {
    closeButton: true,
    debug: false,
    newestOnTop: true,
    progressBar: true,
    positionClass: "toast-top-right",
    preventDuplicates: false,
    showDuration: "1000",
    hideDuration: "1000",
    timeOut: "3000",
    extendedTimeOut: "1000",
    showEasing: "swing",
    hideEasing: "linear",
    showMethod: "fadeIn",
    hideMethod: "fadeOut"
  };

  $("#signup").click(async function(e) {
    e.preventDefault();
    //lay du lieu tu giao dien
    var name = $("#name").val();
    var email = $("#email").val();
    var password = $("#password").val();
    var passagain = $("#passagain").val();
    var role = "user";

    if (name.length == 0) {
      toastr.error("Nhập vào tên bạn");
    } else if (email.length == 0) {
      toastr.error("Nhập vào email");
    } else if (password.length == 0) {
      toastr.error("Nhập vào mật khẩu");
    } else if (passagain.length == 0) {
      toastr.error("Nhập lại mật khẩu");
    } else if (passagain != password) {
      toastr.error("Mật khẩu không khớp");
    } else {
      //lấy kết quả trả về từ API
      const getInfo = () => {
        return new Promise(resolve => {
          $.ajax({
            url: "http://hyperzone.xyz/AppCook/api/register",
            type: "POST",
            dataType: "json",
            data: { email, password, name, role },
            success: function(Result) {
              return resolve(Result);
            },
            error: function(xhr, textStatus, errorThrown) {
              console.log(textStatus);
            }
          });
        });
      };
      //biến chứa kết quả trả về từ API
      const info = await getInfo();
      //kiểm tra kết quả trả về
      if (info.status == false) {
        toastr.error(info.message);
      } else {
        toastr.success("Success sign up!");
        setTimeout(function() {
          window.location.href = "../";
        }, 1500);
      }
    }
  });
});

(function($) {
  "use strict";

  /*==================================================================
    [ Validate ]*/
  var input = $(".validate-input .input100");

  $(".validate-form").on("submit", function() {
    var check = true;

    for (var i = 0; i < input.length; i++) {
      if (validate(input[i]) == false) {
        showValidate(input[i]);
        check = false;
      }
    }

    return check;
  });

  $(".validate-form .input100").each(function() {
    $(this).focus(function() {
      hideValidate(this);
    });
  });

  function validate(input) {
    if ($(input).attr("type") == "email" || $(input).attr("name") == "email") {
      if (
        $(input)
          .val()
          .trim()
          .match(
            /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/
          ) == null
      ) {
        return false;
      }
    } else {
      if (
        $(input)
          .val()
          .trim() == ""
      ) {
        return false;
      }
    }
  }

  function showValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).addClass("alert-validate");
  }

  function hideValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).removeClass("alert-validate");
  }
})(jQuery);
