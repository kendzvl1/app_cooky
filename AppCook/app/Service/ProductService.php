<?php

namespace App\Service;

use App\Models\Product;

class ProductService{

    public function getAll($oderBys=[],$limit=10){

        $query = \DB::table('tb_product')->leftJoin('users','tb_product.author','=','users.id')->select('tb_product.*','users.name');
      //  $query = Product::query();
        if($oderBys){
            $query->orderBy($oderBys['column'],$oderBys['sort']);
        }
        //dd($query);
        return $query->paginate($limit);
    }

 

}
