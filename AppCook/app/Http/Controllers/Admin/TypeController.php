<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Type;
use App\Models\Product;
use Validator;
use App\Service\TypeService;
use Symfony\Component\HttpFoundation\Response;
class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

 
    
    public function index()
    {
        //


        try{

            return response()->json([
                'status'=>true,
                'message'=>'OK',
                'data' => Type::all()
            ],200);

        }catch(\Exception $e){
            return   \response()->json([
                'status'=>false,
                'message'=>$e->getMessage(),
            ]);
        }
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'nametype' => 'required',
        ];    

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return response()->json([
                'status'=>false,
                'message'=>$validator->errors()
            ],200);
        }

        $type = Type::create($request->all());

        return response()->json([
            'status'=>true,
            'message'=>"OK",
            'data'=>$type
        ],200);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {

      //  $test = \DB::table('tb_product')->join('tb_type','tb_product.idtype','=','tb_type.id')->select('tb_type.nametype')->get();
      //  dd($test);


        //
        try{

            $t = Type::find($id);
            if($t==null){
                return \response()->json([
                    'status'=>false,
                    'message'=>'Wrong ID TYPE'
                ]);
            }

            $limit = $request->get('limit') ?? 10;
         //   dd($limit);
            $rs = Product::where('idtype',$id)->orderBy('id','desc')->paginate($limit);
          //  dd($rs);
            return \response()->json([
                'status'=>true,
                'message'=>'OK',
                'data'=>$rs->items(),
                'meta'=>[
                    'total'=>$rs->total(),
                    'perPage'=>$rs->perPage(),
                    'currentPage'=>$rs->currentPage(),
                ]

            ]);

        }
        catch(\Exception $e){

            return   \response()->json([
                'status'=>false,
                'message'=>$e->getMessage(),
            ]);

        }


     //   return \response()->json(Product::get()->where('idtype',$id),200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        //

       

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = [
            'nametype' => 'required',
        ];    

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return response()->json($validator->errors(),200);
        }

        $type = Type::find($id);
        if(!is_null($type)){
            $type->update($request->all());
            return response()->json([
                'status'=>false,
                'message'=>'OK',
                'data'=>$type
            ],200);
        }else{
            return response()->json([
                'status'=>false,
                "message"=>"Record not found !"],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $type = Type::find($id);
        if(!is_null($type)){
            $type->delete();
            return response()->json(null,200);
        }else{
            return response()->json([
                'status'=>false,
                "message"=>"Record not found !"],200);
        }
        
    }
}
