<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;
use Validator;
use App\User;
use Carbon\Carbon;

class Login extends Controller
{
    //

    public function getacc(){
        return response()->json([
            'status'=>true,
            'message'=>'OK',
            'data'=>User::all()
            ],200);
    }

    public function postlogin(Request $request){
     
        $rules = [
            'email' => 'required',
            'password' => 'required',
        ];
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return response()->json(
                [
                    'status'=>false,
                    'message'=>$validator->errors(),
                ]
                ,200);
        }

        $user = User::where('email',$request->email)->first();

        if(!$user){
             return response()->json(
                [
                'status'=>false,
                "message"=>"User not found !"
                ],200);
        }

        if(Hash::check($request->password,$user->password)){
            $user=User::where('email',$request->email)->first();
             return response()->json([
                 'status'=>true,
                 'message'=>"Login OK",
                 'data'=>$user
             ],200);
        }else{
            return response()->json([
                'status'=>false,
                "message"=>"Wrong Password"
                ],200);
        }

        
            
    }

    public function postRegister(Request $request){
        //dangki
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ];    

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return response()->json([
                'status'=>false,
                'message'=>$validator->errors()
            ],200);
        }

        $cc = User::where('email',$request->email)->first();
        if(is_null($cc)){
            $cc = new User();
            $cc->name=$request->name;
            $cc->email=$request->email;
            $cc->role=$request->role;
            $cc->password=Hash::make($request->password);
            $cc->created_at = Carbon::now()->timestamp; 
            $cc->updated_at = Carbon::now()->timestamp; 
            
            $cc->save();
            
            return response()->json([
                'status'=>true,
                'message'=>'OK',
                'data'=>$cc    
            ],200); 
        }else{
            return response()->json([
                'status'=>false,
                "message"=>"Email already exists !"
                ],200);
        }
    }

    public function getoneuser($id){
        $user=User::find($id);
        if(is_null($user)){
            return response()->json(
                
                [   'status'=>false,
                    "message"=>"Record not found !"
                ],200);
        }
        return \response()->json([
            'status:'=>true,
            "message"=>"OK",
            'data'=>$user,
        ],200);
    }


    public function updateuser(Request $request,$id){

        $user = User::find($id);
        $tempupdate=$user;
        
        if(!\is_null($request->name)){
            $tempupdate->name=$request->name;
        }
     
       
        
        if(\is_null($request->oldpassword)){

       
        }else{
            if(Hash::check($request->oldpassword,$user->password)){
               // dd($user->password);
                    $tempupdate->password=Hash::make($request->password);
                  //  $tempupdate->password='kendeptrai';
            
            }else{
                return \response()->json([
                    'status'=>false,
                    'message'=>'Wrong old password !'
                    
                ],200);
            }
        }
        if(is_null($user)){
            return response()->json([
                'status'=>false,
                "message"=>"Record not found !"
                ],200);
        }
       // dd($tempupdate);

       //dd($tempupdate);
       $tempupdate->update((array)$tempupdate->all());
        return response()->json([
                'status'=>true,
                'data'=>$user
        ],200);
        
    }


    public function deleteuser(Request $request,$id){
        $user = User::find($id);

        if(is_null($user)){
            return response()->json([
                'status'=>false,
                'message'=>'Record not found !'
            ],200);
        }

        $user->delete();
        return response()->json([
            'status'=>true,
            'data'=>null
        ],200);
    }

}
