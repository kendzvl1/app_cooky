<?php

namespace App\Http\Controllers\Local;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    //
    public function getlogin(){
        return view('Cook.login');
    }
    public function getregister(){
        return view('Cook.signup');
    }
    public function gethome(){
        return view('Cook.index');
    }
    public function getmypost(){
        return view('Cook.myposts');
    }
    public function getdetail(){
        return view('Cook.detail');
    }
}
