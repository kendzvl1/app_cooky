<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    //
    protected $table ='TB_Type';

    public $timestamps = false;

    protected $fillable=[
        'nametype',
        
    ];
   

}
