<?php

use Illuminate\Database\Seeder;

class Product extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $date = new DateTime();
        $unixTimeStamp = $date->getTimestamp();
        DB::table('TB_Product')->insert([
            [

            'title'=>'bua sang that tuyet',
            'img'=>'day la hinh anh',
            'content'=>'day la content',
            'idtype'=>'1',
            'created_at'=>date('Y-m-d H:i:s')
        ],
        [
            'title'=>'bua sang that tuyet2',
            'img'=>'day la hinh anh2',
            'content'=>'day la content2',
            'idtype'=>'2',
            'created_at'=> date('Y-m-d H:i:s')
        ]
        ]
        );

    }
}
